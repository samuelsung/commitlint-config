# Shared Commitlint Config for Personal Projects

## References
- [conventional-changelog's commitlint repository](https://github.com/conventional-changelog/commitlint)
- [commitlint's rules](https://github.com/conventional-changelog/commitlint/blob/master/docs/reference-rules.md)
