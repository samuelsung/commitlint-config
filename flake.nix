{
  description = "Shared Commitlint Config for Personal Projects";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";

  outputs = { self, nixpkgs, flake-utils, pre-commit-hooks }:
    (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

      in
      rec {
        hook = {
          enable = true;
          name = "commitlint";
          # remote comment before linting
          entry = "${pkgs.writeShellScriptBin "commitlint" ''
            sed '/^#.*/q' "''$@" | ${pkgs.commitlint}/bin/commitlint --config ${./commitlint.config.js}

          ''}/bin/commitlint";
          stages = [ "commit-msg" ];
        };

        checks = {
          pre-commit-check = pre-commit-hooks.lib.${system}.run {
            src = ./.;
            hooks = {
              commitlint = hook;
              nixpkgs-fmt.enable = true;
            };
          };
        };

        devShells.default = pkgs.mkShell {
          inherit (self.checks.${system}.pre-commit-check) shellHook;
        };
      }
    ));
}
